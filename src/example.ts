import Roster from './rostertool';

console.log(`Running RosterTool version: ${Roster.version}`);
console.log(Roster.run(`G`, new Date(), {O: ``}));

for (let i = 1; i <= 31; i++) {
	console.log(i, Roster.run(`7on7DN-Mo1`, new Date(`2023-08-${i.toString().padStart(2,`0`)}`), {O: `x`}));
}

//console.log(Roster.rosterByName(`Roster 6`));
//console.log(Roster.rosterById(1));
//console.log(Roster.crewByName('F'))

//console.log(Roster.who(Roster.rosterByName(`Roster 4`).id, new Date(), `D`));
console.log(Roster.crews(Roster.rosterByName(`Roster 2`)));
console.log(Roster.who(Roster.rosterByName(`Roster 2`),new Date(), `D`));