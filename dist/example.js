"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var rostertool_1 = __importDefault(require("./rostertool"));
console.log("Running RosterTool version: ".concat(rostertool_1.default.version));
console.log("Running RosterTool version: ".concat(rostertool_1.default.version));
console.log(rostertool_1.default.run("G", new Date(), { O: "" }));
for (var i = 1; i <= 31; i++) {
    console.log(i, rostertool_1.default.run("F", new Date("2021-01-".concat(i.toString().padStart(2, "0"))), { O: "x" }));
}
//console.log(Roster.rosterByName(`Roster 6`));
//console.log(Roster.rosterById(1));
//console.log(Roster.crewByName('F'))
//console.log(Roster.who(Roster.rosterByName(`Roster 4`).id, new Date(), `D`));
console.log(rostertool_1.default.crews(rostertool_1.default.rosterByName("Roster 2")));
console.log(rostertool_1.default.who(rostertool_1.default.rosterByName("Roster 2"), new Date(), "D"));
