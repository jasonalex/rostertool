import { ICrew, IOutputs, IPatterns, IRoster, IRosterToolData } from './interface';
import { Data as defaultData } from './data/curragh';
import includes from 'lodash.includes';
const packageInfo = require('../package.json');

interface IData {
	crews: ICrew[];
	patterns: IPatterns;
	rosters: IRoster[];
	epoch: Date;
}

export class RosterToolConstructor{
	public readonly version: string = packageInfo.version;
	public readonly data: IData;

	constructor(data?: IRosterToolData) {
		this.data = {
			epoch: data && data.epoch ? data.epoch : defaultData.epoch,
			crews: data && data.crews || defaultData.crews,
			rosters: data && data.rosters || defaultData.rosters,
			patterns: data && data.patterns || defaultData.patterns
		}
	}

	/**
	 * @param {Date} date1 - the first date to compare
	 * @param {Date} date2 - the second date to repair
	 * @returns {number} - the number of days between date1 and date2
	 */
	protected daysBetween(date1: Date, date2: Date): number {
		var one_day: number = 1000 * 60 * 60 * 24;
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();
		var difference_ms = date2_ms - date1_ms;
		return Math.round(difference_ms / one_day);
	}

	/**
	 * @param crew - the name of the crew to look up
	 * @param lookupDate - the date to look up
	 * @param output - alternative return values to map
	 * @returns output for the crew on that day.
	 */
	public run(crew: any = 'MoFr', lookupDate: Date = new Date(), output?: IOutputs) {
		var crewData = this.data.crews.filter((c) => {
			return (c.displayname === crew || includes(c.names, crew) || includes(c.hiddennames, crew) || c.patterncode === crew);
		}).shift();

		if (crewData) {
			var pattern = this.data.patterns[crewData.patterncode];
			var lookupDate = new Date(lookupDate.setHours(0,0,0,0));
			lookupDate.setDate(lookupDate.getDate());
			var patternIndex = this.daysBetween(this.data.epoch, lookupDate) % pattern.length;
			var shift = pattern[patternIndex];
			if(output === undefined || output[shift] === undefined) {
				return shift;
			}
			return output[shift];
		} else {
			return null;
		}
	}

	/**
	 * What crews are rostered on for a specified date from a specifed roster group
	 * @param rosterId - ID/Code of the roster to lookup 
	 * @param lookupDate - date to lookup
	 * @param shift - shift type to look for (eg: 'D' for dayshift)
	 * @returns array of crew names rostered on
	 */
	public who(rosterId: number | { id: number, [key:string]: any }, lookupDate: Date, shift: string): Array<string> {
		let returnVal: Array<string> = [];

		this.crews(rosterId).forEach((crew) => {
			let todaysShift = this.run(crew, lookupDate);
			(todaysShift === shift) && returnVal.push(crew);
		});

		return returnVal;
	}

	/**
	 * return roster details, lookup by name
	 * @param name - name of the roster to lookup
	 * @returns the roster information object
	 */
	public rosterByName(name: string): IRoster {
		return this.data.rosters.filter((record) => {
			return record.name.display === name || record.name.generic === name || (record.name.others && includes(record.name.others, name))
		})[0];
	}

	/**
	 * return roster details, lookup by Id
	 * @param id 
	 * @returns the roster information object
	 */
	public rosterById(id: number): IRoster {
		return this.data.rosters.filter((record) => {
			return record.id === id;
		})[0];
	}

	/**
	 * return crew details, lookup by name
	 * @param name - name of the crew to lookup
	 * @returns the Crew information object
	 */
	public crewByName(name: string): ICrew {
		return this.data.crews.filter((record) => {
			return (record.displayname === name || record.names && includes(record.names, name)) || (record.hiddennames && includes(record.hiddennames, name));
		})[0];
	}

	public crews(roster: number | { id: number, [key:string]: any }) {
		let id: number;
		if(typeof roster === `object`) {
			id = roster.id;
		} else if (typeof roster === `number`) {
			id = roster;
		} else {
			return [];
		}

		return this.data.crews.filter((record) => {
			return record.rostercode === id;
		}).map((record) => {
			return record.displayname;
		});
	}
}

export default new RosterToolConstructor(defaultData);