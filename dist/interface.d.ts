export interface IRosterToolData {
    name: string;
    epoch: Date;
    crews: ICrew[];
    rosters: IRoster[];
    patterns: IPatterns;
}
export interface ICrew {
    displayname: string;
    names?: Array<string>;
    hiddennames?: Array<string>;
    patterncode: number | string;
    rostercode: number | string;
}
export interface IRoster {
    id: number;
    name: {
        display: string;
        generic: string;
        descriptive: string;
        others?: Array<string>;
    };
    shifts: Array<string>;
    shiftlength: number;
    coverage: 5 | 7;
    type: 'Rotating Shift' | 'Permanent Day Shift';
}
export interface IPatterns {
    [key: string]: Array<string>;
}
export interface IOutputs {
    [key: string]: any;
}
