import { ICrew, IOutputs, IPatterns, IRoster, IRosterToolData } from './interface';
interface IData {
    crews: ICrew[];
    patterns: IPatterns;
    rosters: IRoster[];
    epoch: Date;
}
export declare class RosterToolConstructor {
    readonly version: string;
    readonly data: IData;
    constructor(data?: IRosterToolData);
    /**
     * @param {Date} date1 - the first date to compare
     * @param {Date} date2 - the second date to repair
     * @returns {number} - the number of days between date1 and date2
     */
    protected daysBetween(date1: Date, date2: Date): number;
    /**
     * @param crew - the name of the crew to look up
     * @param lookupDate - the date to look up
     * @param output - alternative return values to map
     * @returns output for the crew on that day.
     */
    run(crew?: any, lookupDate?: Date, output?: IOutputs): any;
    /**
     * What crews are rostered on for a specified date from a specifed roster group
     * @param rosterId - ID/Code of the roster to lookup
     * @param lookupDate - date to lookup
     * @param shift - shift type to look for (eg: 'D' for dayshift)
     * @returns array of crew names rostered on
     */
    who(rosterId: number | {
        id: number;
        [key: string]: any;
    }, lookupDate: Date, shift: string): Array<string>;
    /**
     * return roster details, lookup by name
     * @param name - name of the roster to lookup
     * @returns the roster information object
     */
    rosterByName(name: string): IRoster;
    /**
     * return roster details, lookup by Id
     * @param id
     * @returns the roster information object
     */
    rosterById(id: number): IRoster;
    /**
     * return crew details, lookup by name
     * @param name - name of the crew to lookup
     * @returns the Crew information object
     */
    crewByName(name: string): ICrew;
    crews(roster: number | {
        id: number;
        [key: string]: any;
    }): string[];
}
declare const _default: RosterToolConstructor;
export default _default;
