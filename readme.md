# Rotating Roster Tool

## Installation
```
	npm i rostertool
```


## Usage
### Setup
Use with default setup (Curragh Mine)
```ts
	import Roster from 'rostertool';
```

Use constructor with custom dataset
```ts
	import { RosterToolConstructor } from 'rostertool';
	import { Data as rostertoolData } from 'rostertool/data/curragh';
	const Roster = new RosterToolConstructor(rostertoolData);
```

### Using `Roster.run()`
`Roster.run()` is used to find out what shift a given crew is rostered on a given day.
```ts
	//Roster.run(crew, date[, output])

	// What shift is F crew on today?
	Roster.run("F", new Date());

	// Same information, but with custom outputs for D/N/O
	Roster.run("F", new Date(), {O: `day off`, D: `day shift`, N: `night shift`});
```
Where a custom output has not been defined in the object, the default string from the dataset pattern will be used.  
The built-in dataset only uses `D` (dayshift), `N` (nightshift), `A` (afternoon shift) and `O` (not rostered). However technically any string could be returned by the dataset pattern.

## Custom Datasets
### Built-in
- `rostertool/data/curragh`