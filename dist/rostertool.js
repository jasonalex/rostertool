"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RosterToolConstructor = void 0;
var curragh_1 = require("./data/curragh");
var lodash_includes_1 = __importDefault(require("lodash.includes"));
var packageInfo = require('../package.json');
var RosterToolConstructor = /** @class */ (function () {
    function RosterToolConstructor(data) {
        this.version = packageInfo.version;
        this.data = {
            epoch: data && data.epoch ? data.epoch : curragh_1.Data.epoch,
            crews: data && data.crews || curragh_1.Data.crews,
            rosters: data && data.rosters || curragh_1.Data.rosters,
            patterns: data && data.patterns || curragh_1.Data.patterns
        };
    }
    /**
     * @param {Date} date1 - the first date to compare
     * @param {Date} date2 - the second date to repair
     * @returns {number} - the number of days between date1 and date2
     */
    RosterToolConstructor.prototype.daysBetween = function (date1, date2) {
        var one_day = 1000 * 60 * 60 * 24;
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();
        var difference_ms = date2_ms - date1_ms;
        return Math.round(difference_ms / one_day);
    };
    /**
     * @param crew - the name of the crew to look up
     * @param lookupDate - the date to look up
     * @param output - alternative return values to map
     * @returns output for the crew on that day.
     */
    RosterToolConstructor.prototype.run = function (crew, lookupDate, output) {
        if (crew === void 0) { crew = 'MoFr'; }
        if (lookupDate === void 0) { lookupDate = new Date(); }
        var crewData = this.data.crews.filter(function (c) {
            return (c.displayname === crew || (0, lodash_includes_1.default)(c.names, crew) || (0, lodash_includes_1.default)(c.hiddennames, crew) || c.patterncode === crew);
        }).shift();
        if (crewData) {
            var pattern = this.data.patterns[crewData.patterncode];
            var lookupDate = new Date(lookupDate.setHours(0, 0, 0, 0));
            lookupDate.setDate(lookupDate.getDate());
            var patternIndex = this.daysBetween(this.data.epoch, lookupDate) % pattern.length;
            var shift = pattern[patternIndex];
            if (output === undefined || output[shift] === undefined) {
                return shift;
            }
            return output[shift];
        }
        else {
            return null;
        }
    };
    /**
     * What crews are rostered on for a specified date from a specifed roster group
     * @param rosterId - ID/Code of the roster to lookup
     * @param lookupDate - date to lookup
     * @param shift - shift type to look for (eg: 'D' for dayshift)
     * @returns array of crew names rostered on
     */
    RosterToolConstructor.prototype.who = function (rosterId, lookupDate, shift) {
        var _this = this;
        var returnVal = [];
        this.crews(rosterId).forEach(function (crew) {
            var todaysShift = _this.run(crew, lookupDate);
            (todaysShift === shift) && returnVal.push(crew);
        });
        return returnVal;
    };
    /**
     * return roster details, lookup by name
     * @param name - name of the roster to lookup
     * @returns the roster information object
     */
    RosterToolConstructor.prototype.rosterByName = function (name) {
        return this.data.rosters.filter(function (record) {
            return record.name.display === name || record.name.generic === name || (record.name.others && (0, lodash_includes_1.default)(record.name.others, name));
        })[0];
    };
    /**
     * return roster details, lookup by Id
     * @param id
     * @returns the roster information object
     */
    RosterToolConstructor.prototype.rosterById = function (id) {
        return this.data.rosters.filter(function (record) {
            return record.id === id;
        })[0];
    };
    /**
     * return crew details, lookup by name
     * @param name - name of the crew to lookup
     * @returns the Crew information object
     */
    RosterToolConstructor.prototype.crewByName = function (name) {
        return this.data.crews.filter(function (record) {
            return (record.displayname === name || record.names && (0, lodash_includes_1.default)(record.names, name)) || (record.hiddennames && (0, lodash_includes_1.default)(record.hiddennames, name));
        })[0];
    };
    RosterToolConstructor.prototype.crews = function (roster) {
        var id;
        if (typeof roster === "object") {
            id = roster.id;
        }
        else if (typeof roster === "number") {
            id = roster;
        }
        else {
            return [];
        }
        return this.data.crews.filter(function (record) {
            return record.rostercode === id;
        }).map(function (record) {
            return record.displayname;
        });
    };
    return RosterToolConstructor;
}());
exports.RosterToolConstructor = RosterToolConstructor;
exports.default = new RosterToolConstructor(curragh_1.Data);
