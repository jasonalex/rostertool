import {IRosterToolData} from '../interface';

export const Data: IRosterToolData = {
	name: `Curragh`,
	epoch: new Date(`1899-12-31T00:00:00`),
	crews: [
		{
			displayname: `A`, 
			names: [`4-1`],
			hiddennames: [`4A-1`],
			patterncode: 1,
			rostercode: 5
		},
		{
			displayname: `B`, 
			names: [`4-2`],
			hiddennames: [`4A-2`],
			patterncode: 2,
			rostercode: 5
		},
		{
			displayname: `C`, 
			names: [`4-3`],
			hiddennames: [`4A-3`],
			patterncode: 3,
			rostercode: 5
		},
		{
			displayname: `D`, 
			names: [`5-1`],
			hiddennames: [],
			patterncode: 4,
			rostercode: 3
		},
		{
			displayname: `E`, 
			names: [`5-2`],
			hiddennames: [],
			patterncode: 5,
			rostercode: 3
		},
		{
			displayname: `F`, 
			names: [`6-1`],
			hiddennames: [],
			patterncode: 6,
			rostercode: 1
		},
		{
			displayname: `G`, 
			names: [`6-2`],
			hiddennames: [],
			patterncode: 7,
			rostercode: 1
		},
		{
			displayname: `H`, 
			names: [`6-3`],
			hiddennames: [],
			patterncode: 8,
			rostercode: 1
		},
		{
			displayname: `I`, 
			names: [`6-4`],
			hiddennames: [],
			patterncode: 9,
			rostercode: 1
		},
		{
			displayname: `J`, 
			names: [],
			hiddennames: [],
			patterncode: 10,
			rostercode: 2
		},
		{
			displayname: `K`, 
			names: [],
			hiddennames: [],
			patterncode: 11,
			rostercode: 2
		},
		{
			displayname: `L`, 
			names: [],
			hiddennames: [],
			patterncode: 12,
			rostercode: 2
		},
		{
			displayname: `M`, 
			names: [],
			hiddennames: [],
			patterncode: 13,
			rostercode: 2
		},
		{
			displayname: `P`, 
			names: [],
			hiddennames: [],
			patterncode: 16,
			rostercode: 4
		},
		{
			displayname: `Q`, 
			names: [],
			hiddennames: [],
			patterncode: 17,
			rostercode: 4
		},
		{
			displayname: `2-1`, 
			names: [],
			hiddennames: [`R`],
			patterncode: 18,
			rostercode: 6
		},
		{
			displayname: `2-2`, 
			names: [],
			hiddennames: [`S`],
			patterncode: 19,
			rostercode: 6
		},
		{
			displayname: `2-3`, 
			names: [],
			hiddennames: [`T`],
			patterncode: 20,
			rostercode: 6
		},
		{
			displayname: `MoFr`, 
			names: [`1-1`],
			hiddennames: [`5on2`],
			patterncode: 40,
			rostercode: 14
		},
		{
			displayname: `MoTh`, 
			names: [],
			hiddennames: [],
			patterncode: 41,
			rostercode: 12
		},
		{
			displayname: `TuFr`, 
			names: [],
			hiddennames: [],
			patterncode: 42,
			rostercode: 12
		},
		{
			displayname: `8-1`, 
			names: [],
			hiddennames: [],
			patterncode: 35,
			rostercode: 13
		},
		{
			displayname: `8-2`, 
			names: [],
			hiddennames: [],
			patterncode: 36,
			rostercode: 13
		},
		{
			displayname: `8-3`, 
			names: [],
			hiddennames: [],
			patterncode: 37,
			rostercode: 13
		},
		{
			displayname: `8-4`, 
			names: [],
			hiddennames: [],
			patterncode: 38,
			rostercode: 13
		},
		{
			displayname: `8-5`, 
			names: [],
			hiddennames: [],
			patterncode: 39,
			rostercode: 13
		},
		{
			displayname: `7on7-Mo1`, 
			names: [],
			hiddennames: [],
			patterncode: 58,
			rostercode: 20
		},
		{
			displayname: `7on7-Mo2`, 
			names: [],
			hiddennames: [],
			patterncode: 59,
			rostercode: 20
		},
		{
			displayname: `7on7-Tu1`, 
			names: [],
			hiddennames: [],
			patterncode: 60,
			rostercode: 21
		},
		{
			displayname: `7on7-Tu2`, 
			names: [],
			hiddennames: [],
			patterncode: 61,
			rostercode: 21
		},
		{
			displayname: `7on7-We1`, 
			names: [],
			hiddennames: [],
			patterncode: 62,
			rostercode: 22
		},
		{
			displayname: `7on7-We2`, 
			names: [],
			hiddennames: [],
			patterncode: 63,
			rostercode: 22
		},
		{
			displayname: `7on7-Th1`, 
			names: [],
			hiddennames: [],
			patterncode: 64,
			rostercode: 23
		},
		{
			displayname: `7on7-Th2`, 
			names: [],
			hiddennames: [],
			patterncode: 65,
			rostercode: 23
		},
		{
			displayname: `7on7-Fr1`, 
			names: [],
			hiddennames: [],
			patterncode: 66,
			rostercode: 24
		},
		{
			displayname: `7on7-Fr2`, 
			names: [],
			hiddennames: [],
			patterncode: 67,
			rostercode: 24
		},
		{
			displayname: `7on7-Sa1`, 
			names: [],
			hiddennames: [],
			patterncode: 68,
			rostercode: 25
		},
		{
			displayname: `7on7-Sa2`, 
			names: [],
			hiddennames: [],
			patterncode: 69,
			rostercode: 25
		},
		{
			displayname: `7on7-Su1`, 
			names: [],
			hiddennames: [],
			patterncode: 70,
			rostercode: 26
		},
		{
			displayname: `7on7-Su2`, 
			names: [],
			hiddennames: [],
			patterncode: 71,
			rostercode: 26
		},
		{
			displayname: `W9S-1`,
			names: [],
			hiddennames: [],
			patterncode: 72,
			rostercode: 27
		},
		{
			displayname: `W9S-2`,
			names: [],
			hiddennames: [],
			patterncode: 73,
			rostercode: 27
		},
		{
			displayname: `W9S-3`,
			names: [],
			hiddennames: [],
			patterncode: 74,
			rostercode: 27
		},
		{
			displayname: `W9S-4`,
			names: [],
			hiddennames: [],
			patterncode: 75,
			rostercode: 27
		},

		{
			displayname: `7on7DN-Mo1`,
			names: [],
			hiddennames: [],
			patterncode: 79,
			rostercode: 29
		},
		{
			displayname: `7on7DN-Mo2`,
			names: [],
			hiddennames: [],
			patterncode: 80,
			rostercode: 29
		},
		{
			displayname: `7on7DN-Mo3`,
			names: [],
			hiddennames: [],
			patterncode: 81,
			rostercode: 29
		},
		{
			displayname: `7on7DN-Mo4`,
			names: [],
			hiddennames: [],
			patterncode: 82,
			rostercode: 29
		},
		{
			displayname: `7on7DN-Tu1`,
			names: [],
			hiddennames: [],
			patterncode: 83,
			rostercode: 30
		},
		{
			displayname: `7on7DN-Tu2`,
			names: [],
			hiddennames: [],
			patterncode: 84,
			rostercode: 30
		},
		{
			displayname: `7on7DN-Tu3`,
			names: [],
			hiddennames: [],
			patterncode: 85,
			rostercode: 30
		},
		{
			displayname: `7on7DN-Tu4`,
			names: [],
			hiddennames: [],
			patterncode: 86,
			rostercode: 30
		},
		{
			displayname: `7on7DN-We1`,
			names: [],
			hiddennames: [],
			patterncode: 87,
			rostercode: 31
		},
		{
			displayname: `7on7DN-We2`,
			names: [],
			hiddennames: [],
			patterncode: 88,
			rostercode: 31
		},
		{
			displayname: `7on7DN-We3`,
			names: [],
			hiddennames: [],
			patterncode: 89,
			rostercode: 31
		},
		{
			displayname: `7on7DN-We4`,
			names: [],
			hiddennames: [],
			patterncode: 90,
			rostercode: 31
		},
		{
			displayname: `7on7DN-Th1`,
			names: [],
			hiddennames: [],
			patterncode: 91,
			rostercode: 32
		},
		{
			displayname: `7on7DN-Th2`,
			names: [],
			hiddennames: [],
			patterncode: 92,
			rostercode: 32
		},
		{
			displayname: `7on7DN-Th3`,
			names: [],
			hiddennames: [],
			patterncode: 93,
			rostercode: 32
		},
		{
			displayname: `7on7DN-Th4`,
			names: [],
			hiddennames: [],
			patterncode: 94,
			rostercode: 32
		},
		{
			displayname: `7on7DN-Fr1`,
			names: [],
			hiddennames: [],
			patterncode: 95,
			rostercode: 33
		},
		{
			displayname: `7on7DN-Fr2`,
			names: [],
			hiddennames: [],
			patterncode: 96,
			rostercode: 33
		},
		{
			displayname: `7on7DN-Fr3`,
			names: [],
			hiddennames: [],
			patterncode: 97,
			rostercode: 33
		},
		{
			displayname: `7on7DN-Fr4`,
			names: [],
			hiddennames: [],
			patterncode: 98,
			rostercode: 33
		},
		{
			displayname: `7on7DN-Sa1`,
			names: [],
			hiddennames: [],
			patterncode: 99,
			rostercode: 34
		},
		{
			displayname: `7on7DN-Sa2`,
			names: [],
			hiddennames: [],
			patterncode: 100,
			rostercode: 34
		},
		{
			displayname: `7on7DN-Sa3`,
			names: [],
			hiddennames: [],
			patterncode: 101,
			rostercode: 34
		},
		{
			displayname: `7on7DN-Sa4`,
			names: [],
			hiddennames: [],
			patterncode: 102,
			rostercode: 34
		},
		{
			displayname: `7on7DN-Su1`,
			names: [],
			hiddennames: [],
			patterncode: 103,
			rostercode: 35
		},
		{
			displayname: `7on7DN-Su2`,
			names: [],
			hiddennames: [],
			patterncode: 104,
			rostercode: 35
		},
		{
			displayname: `7on7DN-Su3`,
			names: [],
			hiddennames: [],
			patterncode: 105,
			rostercode: 35
		},
		{
			displayname: `7on7DN-Su4`,
			names: [],
			hiddennames: [],
			patterncode: 106,
			rostercode: 35
		},

		{
			displayname: `8on6-Mo1`, 
			names: [],
			hiddennames: [],
			patterncode: 107,
			rostercode: 36
		},
		{
			displayname: `8on6-Mo2`, 
			names: [],
			hiddennames: [],
			patterncode: 108,
			rostercode: 36
		},
		{
			displayname: `8on6-Tu1`, 
			names: [],
			hiddennames: [],
			patterncode: 109,
			rostercode: 37
		},
		{
			displayname: `8on6-Tu2`, 
			names: [],
			hiddennames: [],
			patterncode: 110,
			rostercode: 37
		},
		{
			displayname: `8on6-We1`, 
			names: [],
			hiddennames: [],
			patterncode: 111,
			rostercode: 38
		},
		{
			displayname: `8on6-We2`, 
			names: [],
			hiddennames: [],
			patterncode: 112,
			rostercode: 38
		},
		{
			displayname: `8on6-Th1`, 
			names: [],
			hiddennames: [],
			patterncode: 113,
			rostercode: 39
		},
		{
			displayname: `8on6-Th2`, 
			names: [],
			hiddennames: [],
			patterncode: 114,
			rostercode: 39
		},
		{
			displayname: `8on6-Fr1`, 
			names: [],
			hiddennames: [],
			patterncode: 115,
			rostercode: 40
		},
		{
			displayname: `8on6-Fr2`, 
			names: [],
			hiddennames: [],
			patterncode: 116,
			rostercode: 40
		},
		{
			displayname: `8on6-Sa1`, 
			names: [],
			hiddennames: [],
			patterncode: 117,
			rostercode: 41
		},
		{
			displayname: `8on6-Sa2`, 
			names: [],
			hiddennames: [],
			patterncode: 118,
			rostercode: 41
		},
		{
			displayname: `8on6-Su1`, 
			names: [],
			hiddennames: [],
			patterncode: 119,
			rostercode: 42
		},
		{
			displayname: `8on6-Su2`, 
			names: [],
			hiddennames: [],
			patterncode: 120,
			rostercode: 42
		},

		{
			displayname: `8on6DN-Mo1`,
			names: [],
			hiddennames: [],
			patterncode: 121,
			rostercode: 43
		},
		{
			displayname: `8on6DN-Mo2`,
			names: [],
			hiddennames: [],
			patterncode: 122,
			rostercode: 43
		},
		{
			displayname: `8on6DN-Mo3`,
			names: [],
			hiddennames: [],
			patterncode: 123,
			rostercode: 43
		},
		{
			displayname: `8on6DN-Mo4`,
			names: [],
			hiddennames: [],
			patterncode: 124,
			rostercode: 43
		},
		{
			displayname: `8on6DN-Tu1`,
			names: [],
			hiddennames: [],
			patterncode: 125,
			rostercode: 44
		},
		{
			displayname: `8on6DN-Tu2`,
			names: [],
			hiddennames: [],
			patterncode: 126,
			rostercode: 44
		},
		{
			displayname: `8on6DN-Tu3`,
			names: [],
			hiddennames: [],
			patterncode: 127,
			rostercode: 44
		},
		{
			displayname: `8on6DN-Tu4`,
			names: [],
			hiddennames: [],
			patterncode: 128,
			rostercode: 44
		},
		{
			displayname: `8on6DN-We1`,
			names: [],
			hiddennames: [],
			patterncode: 129,
			rostercode: 45
		},
		{
			displayname: `8on6DN-We2`,
			names: [],
			hiddennames: [],
			patterncode: 130,
			rostercode: 45
		},
		{
			displayname: `8on6DN-We3`,
			names: [],
			hiddennames: [],
			patterncode: 131,
			rostercode: 45
		},
		{
			displayname: `8on6DN-We4`,
			names: [],
			hiddennames: [],
			patterncode: 132,
			rostercode: 45
		},
		{
			displayname: `8on6DN-Th1`,
			names: [],
			hiddennames: [],
			patterncode: 133,
			rostercode: 46
		},
		{
			displayname: `8on6DN-Th2`,
			names: [],
			hiddennames: [],
			patterncode: 134,
			rostercode: 46
		},
		{
			displayname: `8on6DN-Th3`,
			names: [],
			hiddennames: [],
			patterncode: 135,
			rostercode: 46
		},
		{
			displayname: `8on6DN-Th4`,
			names: [],
			hiddennames: [],
			patterncode: 136,
			rostercode: 46
		},
		{
			displayname: `8on6DN-Fr1`,
			names: [],
			hiddennames: [],
			patterncode: 137,
			rostercode: 47
		},
		{
			displayname: `8on6DN-Fr2`,
			names: [],
			hiddennames: [],
			patterncode: 138,
			rostercode: 47
		},
		{
			displayname: `8on6DN-Fr3`,
			names: [],
			hiddennames: [],
			patterncode: 139,
			rostercode: 47
		},
		{
			displayname: `8on6DN-Fr4`,
			names: [],
			hiddennames: [],
			patterncode: 140,
			rostercode: 47
		},
		{
			displayname: `8on6DN-Sa1`,
			names: [],
			hiddennames: [],
			patterncode: 141,
			rostercode: 48
		},
		{
			displayname: `8on6DN-Sa2`,
			names: [],
			hiddennames: [],
			patterncode: 142,
			rostercode: 48
		},
		{
			displayname: `8on6DN-Sa3`,
			names: [],
			hiddennames: [],
			patterncode: 143,
			rostercode: 48
		},
		{
			displayname: `8on6DN-Sa4`,
			names: [],
			hiddennames: [],
			patterncode: 144,
			rostercode: 48
		},
		{
			displayname: `8on6DN-Su1`,
			names: [],
			hiddennames: [],
			patterncode: 145,
			rostercode: 49
		},
		{
			displayname: `8on6DN-Su2`,
			names: [],
			hiddennames: [],
			patterncode: 146,
			rostercode: 49
		},
		{
			displayname: `8on6DN-Su3`,
			names: [],
			hiddennames: [],
			patterncode: 147,
			rostercode: 49
		},
		{
			displayname: `8on6DN-Su4`,
			names: [],
			hiddennames: [],
			patterncode: 148,
			rostercode: 49
		},

	],
	rosters: [
		{
			id: 1,
			name: {
				generic: `Roster 6`,
				descriptive: `7 Day 12.5 Hour Shift Roster`,
				display: `Roster 6`,
				others: [`6`, `Roster6`, `EBA2012-Roster6`, `EA2015-Roster6`, `EA2019-Roster6`]
			},
			shifts: [`D`, `N`],
			shiftlength: 12.5,
			coverage: 7,
			type: `Rotating Shift`
		},
	
		{
			id: 2,
			name: {
				generic: `FMaint-Specific`,
				descriptive: `7 Day 12.5 Hour Shift Roster [Field Maintenance ONLY]`,
				display: `Field Maintenance Specific`
			},
			shifts: [`D`, `N`],
			shiftlength: 12.5,
			coverage: 7,
			type: `Rotating Shift`
		},
	
		{
			id: 3,
			name: {
				generic: `Roster5`,
				descriptive: `7 Day Permanent Day 12 Hour Shift Roster`,
				display: `Roster 5`,
				others: [`Roster5`, `5`, `EBA2012-Roster5`, `EA2015-Roster5`, `EA2019-Roster5`]
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 4,
			name: {
				generic: `Roster5-Alt`,
				descriptive: `7 Day Permanent Day 12 Hour Shift Roster [Alternative]`,
				display: `Roster 5 (Alt)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 5,
			name: {
				generic: `Roster4`,
				descriptive: `5 Day 12.5 Hour Shift Roster`,
				display: `Roster 4`,
				others: [`Roster4`, `4`, `EBA2012-Roster4`, `EA2015-Roster4`, `EA2019-Roster4`],
			},
			shifts: [`D` ,`N`],
			shiftlength: 12.5,
			coverage: 5,
			type: `Rotating Shift`
		},
	
		{
			id: 6,
			name: {
				generic: `Roster2`,
				descriptive: `5 Day Permanent Day 12 Hour Shift Roster`,
				display: `Roster 2`,
				others: [`Roster2`, `2`, `EBA2012-Roster2`, `EA2015-Roster2`, `EA2019-Roster2`]
			},
			shifts: [`D`],
			shiftlength: 12.5,
			coverage: 5,
			type: `Permanent Day Shift`
		},
	
		{
			id: 12,
			name: {
				generic: `GenericWeekday`,
				descriptive: `Generic Weekday Rosters`,
				display: `Generic Weekday Rosters`
			},
			shifts: [`D`],
			shiftlength: 8,
			coverage: 5,
			type: `Permanent Day Shift`
		},
	
		{
			id: 13,
			name: {
				generic: `Roster8`,
				descriptive: `7 Day 12.5 Hour Shift Roster`,
				display: `Roster 8`,
				others: [`Roster 8`, `8`, `EBA2012-Roster8`, `EA2015-Roster8`, `EA2019-Roster8`]
			},
			shifts: [`D`, `N`],
			shiftlength: 12.5,
			coverage: 7,
			type: `Rotating Shift`
		},
	
		{
			id: 14,
			name: {
				generic: `Roster1`,
				descriptive: `5 Day Permanent Day 8 Hour Shift Roster`,
				display: `Roster 1`,
				others: [`Roster 1`, `1`, `EBA2012-Roster1`, `EA2015-Roster1`, `EA2019-Roster1`]
			},
			shifts: [`D`],
			shiftlength: 12.5,
			coverage: 7,
			type: `Rotating Shift`
		},
	
		{
			id: 20,
			name: {
				generic: `7-on-7-off-Mondays`,
				descriptive: `7-on-7-off (starting Mondays)`,
				display: `7-on-7-off (starting Mondays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},	
	
		{
			id: 21,
			name: {
				generic: `7-on-7-off-Tuesdays`,
				descriptive: `7-on-7-off (starting Tuesdays)`,
				display: `7-on-7-off (starting Tuesdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},	
	
		{
			id: 22,
			name: {
				generic: `7-on-7-off-Wednesdays`,
				descriptive: `7-on-7-off (starting Wednesdays)`,
				display: `7-on-7-off (starting Wednesdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},	
	
		{
			id: 23,
			name: {
				generic: `7-on-7-off-Thursdays`,
				descriptive: `7-on-7-off (starting Thursdays)`,
				display: `7-on-7-off (starting Thursdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 24,
			name: {
				generic: `7-on-7-off-Fridays`,
				descriptive: `7-on-7-off (starting Fridays)`,
				display: `7-on-7-off (starting Fridays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 25,
			name: {
				generic: `7-on-7-off-Saturdays`,
				descriptive: `7-on-7-off (starting Saturdays)`,
				display: `7-on-7-off (starting Saturdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 26,
			name: {
				generic: `7-on-7-off-Sundays`,
				descriptive: `7-on-7-off (starting Sundays)`,
				display: `7-on-7-off (starting Sundays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},

		{
			id: 27,
			name: {
				generic: `RosterW9S`,
				descriptive: `Warehouse 9S Roster`,
				display: `Warehouse 9S Roster`
			},
			shifts: [`D`, `N`],
			shiftlength: 12.5,
			coverage: 7,
			type: `Rotating Shift`
		},

		{
			id: 29,
			name: {
				generic: `7-on-7-DayNight-Monday`,
				descriptive: `7-on-7-off Day Night (starting Monday)`,
				display: `7-on-7-off Day Night (starting Monday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 30,
			name: {
				generic: `7-on-7-DayNight-Tuesday`,
				descriptive: `7-on-7-off Day Night (starting Tuesday)`,
				display: `7-on-7-off Day Night (starting Tuesday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 31,
			name: {
				generic: `7-on-7-DayNight-Wednesday`,
				descriptive: `7-on-7-off Day Night (starting Wednesday)`,
				display: `7-on-7-off Day Night (starting Wednesday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 32,
			name: {
				generic: `7-on-7-DayNight-Thursday`,
				descriptive: `7-on-7-off Day Night (starting Thursday)`,
				display: `7-on-7-off Day Night (starting Thursday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 33,
			name: {
				generic: `7-on-7-DayNight-Friday`,
				descriptive: `7-on-7-off Day Night (starting Friday)`,
				display: `7-on-7-off Day Night (starting Friday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 34,
			name: {
				generic: `7-on-7-DayNight-Saturday`,
				descriptive: `7-on-7-off Day Night (starting Saturday)`,
				display: `7-on-7-off Day Night (starting Saturday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 35,
			name: {
				generic: `7-on-7-DayNight-Sunday`,
				descriptive: `7-on-7-off Day Night (starting Sunday)`,
				display: `7-on-7-off Day Night (starting Sunday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 36,
			name: {
				generic: `8-on-6-off-Mondays`,
				descriptive: `8-on-6-off (starting Mondays)`,
				display: `8-on-6-off (starting Mondays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},	
	
		{
			id: 37,
			name: {
				generic: `8-on-6-off-Tuesdays`,
				descriptive: `8-on-6-off (starting Tuesdays)`,
				display: `8-on-6-off (starting Tuesdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},	
	
		{
			id: 38,
			name: {
				generic: `8-on-6-off-Wednesdays`,
				descriptive: `8-on-6-off (starting Wednesdays)`,
				display: `8-on-6-off (starting Wednesdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},	
	
		{
			id: 39,
			name: {
				generic: `8-on-6-off-Thursdays`,
				descriptive: `8-on-6-off (starting Thursdays)`,
				display: `8-on-6-off (starting Thursdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 40,
			name: {
				generic: `8-on-6-off-Fridays`,
				descriptive: `8-on-6-off (starting Fridays)`,
				display: `8-on-6-off (starting Fridays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 41,
			name: {
				generic: `8-on-6-off-Saturdays`,
				descriptive: `8-on-6-off (starting Saturdays)`,
				display: `8-on-6-off (starting Saturdays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
	
		{
			id: 42,
			name: {
				generic: `8-on-6-off-Sundays`,
				descriptive: `8-on-6-off (starting Sundays)`,
				display: `8-on-6-off (starting Sundays)`
			},
			shifts: [`D`],
			shiftlength: 12,
			coverage: 7,
			type: `Permanent Day Shift`
		},
		
		{
			id: 43,
			name: {
				generic: `8-on-6-DayNight-Monday`,
				descriptive: `8-on-6-off Day Night (starting Monday)`,
				display: `8-on-6-off Day Night (starting Monday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 44,
			name: {
				generic: `8-on-6-DayNight-Tuesday`,
				descriptive: `8-on-6-off Day Night (starting Tuesday)`,
				display: `8-on-6-off Day Night (starting Tuesday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 45,
			name: {
				generic: `8-on-6-DayNight-Wednesday`,
				descriptive: `8-on-6-off Day Night (starting Wednesday)`,
				display: `8-on-6-off Day Night (starting Wednesday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 46,
			name: {
				generic: `8-on-6-DayNight-Thursday`,
				descriptive: `8-on-6-off Day Night (starting Thursday)`,
				display: `8-on-6-off Day Night (starting Thursday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 47,
			name: {
				generic: `8-on-6-DayNight-Friday`,
				descriptive: `8-on-6-off Day Night (starting Friday)`,
				display: `8-on-6-off Day Night (starting Friday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 48,
			name: {
				generic: `8-on-6-DayNight-Saturday`,
				descriptive: `8-on-6-off Day Night (starting Saturday)`,
				display: `8-on-6-off Day Night (starting Saturday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
		{
			id: 49,
			name: {
				generic: `8-on-6-DayNight-Sunday`,
				descriptive: `8-on-6-off Day Night (starting Sunday)`,
				display: `8-on-6-off Day Night (starting Sunday)`
			},
			shifts: [`D`, `N`],
			shiftlength: 12,
			coverage: 7,
			type: `Rotating Shift`
		},
	],
	patterns: {
		/* A (4-1) */ 1: [`O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`],
		/* B (4-2) */ 2: [`O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`],
		/* C (4-3) */ 3: [`O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `N`, `N`, `N`, `O`, `O`, `O`],
		/* D (5-1) */ 4: [`D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`],
		/* E (5-2) */ 5: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`],
		/* F (6-1) */ 6: [`N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`],
		/* G (6-2) */ 7: [`O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`],
		/* H (6-3) */ 8: [`O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`],
		/* I (6-4) */ 9: [`D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `D`, `D`],
		/* J */ 10: [`D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`],
		/* K */ 11: [`N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `N`, `N`],
		/* L */ 12: [`O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`],
		/* M */ 13: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`],
		/* P */ 16: [`D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`],
		/* Q */ 17: [`O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`],
		/* R (2-1) */ 18: [`O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `D`, `D`, `O`],
		/* S (2-2) */ 19: [`O`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`],
		/* T (2-3) */ 20: [`O`, `D`, `D`, `O`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`],
		 21: [`O`, `A`, `A`, `A`, `A`, `A`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`],
		22: [`O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `A`, `A`, `A`, `A`, `A`, `O`],
		23: [`O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `A`, `A`, `A`, `A`, `A`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`],
		/*  */ 27: [`O`, `O`, `A`, `A`, `A`, `A`, `A`, `A`, `A`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`],
		28: [`D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `A`, `A`, `A`, `A`, `A`, `A`, `A`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`],
		29: [`O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `A`, `A`, `A`, `A`, `A`, `A`, `A`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`],
		30: [`N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `A`, `A`, `A`, `A`, `A`, `A`, `A`, `O`, `O`, `O`, `N`, `N`],
		31: [`A`, `A`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `A`, `A`, `A`, `A`, `A`],
		32: [`O`, `D`, `D`, `D`, `D`, `O`, `O`],
		33: [`O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`],
		34: [`O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`],
		/* 8-1 */ 35: [`D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`],
		/* 8-2 */ 36: [`O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`],
		/* 8-3 */ 37: [`N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`],
		/* 8-4 */ 38: [`O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`],
		/* 8-5 */ 39: [`O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`],
		/* MoFr (1-1) */ 40: [`O`, `D`, `D`, `D`, `D`, `D`, `O`],
		/* MoTh */ 41: [`O`, `D`, `D`, `D`, `D`, `O`, `O`],
		/* TuFr */ 42: [`O`, `O`, `D`, `D`, `D`, `D`, `O`],
		43: [`D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`],
		44: [`O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`],
		45: [`N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`],
		46: [`D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`],
		47: [`O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `O`],
		48: [`D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`],
		49: [`O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`],
		50: [`N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`],
		51: [`O`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`],
		52: [`O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `O`, `N`, `N`, `O`],
		53: [`O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`],
		54: [`D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`],
		55: [`O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`],
		56: [`O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`],
		57: [`O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`],
		/* 7on7-Mo1 */ 58: [`O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`],
		/* 7on7-Mo2 */ 59: [`D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`],
		/* 7on7-Tu1 */ 60: [`O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`],
		/* 7on7-Tu2 */ 61: [`D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`],
		/* 7on7-We1 */ 62: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`],
		/* 7on7-We2 */ 63: [`D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`],
		/* 7on7-Th1 */ 64: [`O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`],
		/* 7on7-Th2 */ 65: [`D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`],
		/* 7on7-Fr1 */ 66: [`O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`],
		/* 7on7-Fr2 */ 67: [`D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`],
		/* 7on7-Sa1 */ 68: [`O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`],
		/* 7on7-Sa2 */ 69: [`D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`],
		/* 7on7-Su1 */ 70: [`O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`],
		/* 7on7-Su2 */ 71: [`D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`],
		/* W9S-1 */ 72: [`D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`],
		/* W9S-2 */73: [`N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`],
		/* W9S-3 */74: [`O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`],
		/* W9S-4 */75: [`O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`],
		76: [`O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`],
		77: [`O`, `O`, `D`, `D`, `D`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`],
		78: [`O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `D`, `D`, `D`, `O`, `O`],
        /* 7on7DN-Mo1 */ 79: [`N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`],
        /* 7on7DN-Mo2 */ 80: [`O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`],
        /* 7on7DN-Mo3 */ 81: [`D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`],
        /* 7on7DN-Mo4 */ 82: [`O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`],
        /* 7on7DN-Tu1 */ 83: [`N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`],
        /* 7on7DN-Tu2 */ 84: [`O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`],
        /* 7on7DN-Tu3 */ 85: [`D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`],
        /* 7on7DN-Tu4 */ 86: [`O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`],
        /* 7on7DN-We1 */ 87: [`N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`],
        /* 7on7DN-We2 */ 88: [`O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`],
        /* 7on7DN-We3 */ 89: [`D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`],
        /* 7on7DN-We4 */ 90: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`],
        /* 7on7DN-Th1 */ 91: [`N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`],
        /* 7on7DN-Th2 */ 92: [`O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`],
        /* 7on7DN-Th3 */ 93: [`D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`],
        /* 7on7DN-Th4 */ 94: [`O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`],
        /* 7on7DN-Fr1 */ 95: [`N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`],
        /* 7on7DN-Fr2 */ 96: [`O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`],
        /* 7on7DN-Fr3 */ 97: [`D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`],
        /* 7on7DN-Fr4 */ 98: [`O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`],
        /* 7on7DN-Sa1 */ 99: [`N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`],
        /* 7on7DN-Sa2 */ 100: [`O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`],
        /* 7on7DN-Sa3 */ 101: [`D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`],
        /* 7on7DN-Sa4 */ 102: [`O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`],
        /* 7on7DN-Su1 */ 103: [`N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`],
        /* 7on7DN-Su2 */ 104: [`O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`],
        /* 7on7DN-Su3 */ 105: [`D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `O`],
        /* 7on7DN-Su4 */ 106: [`O`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`],

		/* 8on6-Mo1 */ 107: [`O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`],
		/* 8on6-Mo2 */ 108: [`D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`],
		/* 8on6-Tu1 */ 109: [`O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`],
		/* 8on6-Tu2 */ 110: [`D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`],
		/* 8on6-We1 */ 111: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`],
		/* 8on6-We2 */ 112: [`D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`],
		/* 8on6-Th1 */ 113: [`O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`],
		/* 8on6-Th2 */ 114: [`D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`],
		/* 8on6-Fr1 */ 115: [`O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`],
		/* 8on6-Fr2 */ 116: [`D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`],
		/* 8on6-Sa1 */ 117: [`O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`],
		/* 8on6-Sa2 */ 118: [`D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`],
		/* 8on6-Su1 */ 119: [`D`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`],
		/* 8on6-Su2 */ 120: [`D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`],

 		/* 8on6DN-Mo1 */ 121: [`N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`],
        /* 8on6DN-Mo2 */ 122: [`O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`],
        /* 8on6DN-Mo3 */ 123: [`D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`],
        /* 8on6DN-Mo4 */ 124: [`O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`],
        /* 8on6DN-Tu1 */ 125: [`N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`],
        /* 8on6DN-Tu2 */ 126: [`O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`],
        /* 8on6DN-Tu3 */ 127: [`D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`],
        /* 8on6DN-Tu4 */ 128: [`O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`],
        /* 8on6DN-We1 */ 129: [`N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`],
        /* 8on6DN-We2 */ 130: [`O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`],
        /* 8on6DN-We3 */ 131: [`D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`],
        /* 8on6DN-We4 */ 132: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`],
        /* 8on6DN-Th1 */ 133: [`N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`],
        /* 8on6DN-Th2 */ 134: [`O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`],
        /* 8on6DN-Th3 */ 135: [`D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`],
        /* 8on6DN-Th4 */ 136: [`O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`],
        /* 8on6DN-Fr1 */ 137: [`N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`],
        /* 8on6DN-Fr2 */ 138: [`O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`],
        /* 8on6DN-Fr3 */ 139: [`D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`],
        /* 8on6DN-Fr4 */ 140: [`O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`],
        /* 8on6DN-Sa1 */ 141: [`N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`],
        /* 8on6DN-Sa2 */ 142: [`O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`],
        /* 8on6DN-Sa3 */ 143: [`D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`],
        /* 8on6DN-Sa4 */ 144: [`O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`],
        /* 8on6DN-Su1 */ 145: [`N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`],
        /* 8on6DN-Su2 */ 146: [`D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`],
        /* 8on6DN-Su3 */ 147: [`D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `N`, `O`, `O`, `O`, `O`, `O`, `O`],
        /* 8on6DN-Su4 */ 148: [`N`, `O`, `O`, `O`, `O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`, `O`, `O`, `O`, `N`, `N`, `N`, `N`, `N`, `N`, `N`],
		9001: [`O`, `O`, `O`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `D`, `O`, `O`, `O`]
	} 
}
